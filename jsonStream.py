import os
import json

def jsonStream(file, container_type=list):
  not_first = False

  with open(file, 'w') as f:
      json.dump(container_type(), f)

  if container_type == list:
    def add(to_append):
      with open(file, 'r+') as f:
        f.seek(f.seek(0, os.SEEK_END) - 1)
        nonlocal not_first
        if not_first: f.write(',')
        else: not_first = True
        json.dump(to_append, f)
        f.write(']')
      return add

  else:
    raise NotImplementedError('container_type %s is not yet supported.' % container_type)

  return add

