import unittest
import os
import json

import jsonStream

class jsonStreamTestCases(unittest.TestCase):
  filename = 'test.json'

  def tearDown(self):
    try:
      os.remove(self.filename)
    except OSError:
      pass

  def test_list(self):

    stream = jsonStream.jsonStream(self.filename)
    to_stream = ["i","am","awesome!","rad"]
    for data in to_stream: stream(data)
    with open(self.filename) as f:
      self.assertEqual(to_stream, json.load(f))


if __name__ == '__main__': unittest.main()

